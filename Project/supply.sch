EESchema Schematic File Version 4
LIBS:pcb_embedded_systems-cache
EELAYER 26 0
EELAYER END
$Descr A4 8268 11693 portrait
encoding utf-8
Sheet 4 5
Title "SUPPLY"
Date ""
Rev "JaiberC"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 5600 4050 2    50   Input ~ 0
+3.3V_MCU
Text GLabel 5450 6350 2    50   Input ~ 0
+3.3V_FPGA
$Comp
L power:GND #PWR019
U 1 1 5B8E8CA1
P 2750 4800
F 0 "#PWR019" H 2750 4550 50  0001 C CNN
F 1 "GND" H 2755 4627 50  0000 C CNN
F 2 "" H 2750 4800 50  0001 C CNN
F 3 "" H 2750 4800 50  0001 C CNN
	1    2750 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 4800 2750 4650
$Comp
L power:GND #PWR022
U 1 1 5B8E8D2D
P 3650 4850
F 0 "#PWR022" H 3650 4600 50  0001 C CNN
F 1 "GND" H 3655 4677 50  0000 C CNN
F 2 "" H 3650 4850 50  0001 C CNN
F 3 "" H 3650 4850 50  0001 C CNN
	1    3650 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 4700 3650 4850
$Comp
L Device:C C?
U 1 1 5B8E8E38
P 4800 4350
AR Path="/5B7CD4BD/5B8E8E38" Ref="C?"  Part="1" 
AR Path="/5B7CD50C/5B8E8E38" Ref="C28"  Part="1" 
F 0 "C28" H 4900 4350 50  0000 L CNN
F 1 "0.1uF" H 4850 4250 50  0000 L CNN
F 2 "modules:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4838 4200 50  0001 C CNN
F 3 "~" H 4800 4350 50  0001 C CNN
	1    4800 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR025
U 1 1 5B8E8F11
P 4800 4700
F 0 "#PWR025" H 4800 4450 50  0001 C CNN
F 1 "GND" H 4805 4527 50  0000 C CNN
F 2 "" H 4800 4700 50  0001 C CNN
F 3 "" H 4800 4700 50  0001 C CNN
	1    4800 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 4500 4800 4700
Wire Wire Line
	4800 4200 4800 4050
Wire Wire Line
	5200 4050 5600 4050
Text GLabel 5600 8600 2    50   Input ~ 0
+1.2V_FPGA
Wire Wire Line
	2250 4200 2750 4200
Wire Wire Line
	2750 4350 2750 4200
Connection ~ 2750 4200
Wire Wire Line
	2750 4200 2950 4200
Text Notes 3500 3450 0    50   ~ 10
POWER SUPPLY +3.3VDC\nMCU_ST_STM32F446ZET6\nONION OMEGA 1
Wire Notes Line
	700  5500 8050 5500
Text Notes 3100 5800 0    50   ~ 10
POWER SUPPLY +3.3VDC\nFPGA_Lattice_ICE40HX1K
Wire Notes Line
	500  7700 7850 7700
Text Notes 4400 8000 2    50   ~ 10
POWER SUPPLY +1.2VDC\nFPGA_Lattice_ICE40HX1K
Text GLabel 2250 8750 0    50   Input ~ 0
+5V
Wire Wire Line
	5200 4500 5200 4700
Wire Wire Line
	5200 4200 5200 4050
$Comp
L power:GND #PWR028
U 1 1 5B985526
P 5200 4700
F 0 "#PWR028" H 5200 4450 50  0001 C CNN
F 1 "GND" H 5205 4527 50  0000 C CNN
F 2 "" H 5200 4700 50  0001 C CNN
F 3 "" H 5200 4700 50  0001 C CNN
	1    5200 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 4050 5200 4050
Connection ~ 4800 4050
Connection ~ 5200 4050
$Comp
L Device:C C?
U 1 1 5B987070
P 2600 6800
AR Path="/5B7CD4BD/5B987070" Ref="C?"  Part="1" 
AR Path="/5B7CD50C/5B987070" Ref="C23"  Part="1" 
F 0 "C23" H 2700 6800 50  0000 L CNN
F 1 "33uF" H 2650 6700 50  0000 L CNN
F 2 "modules:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2638 6650 50  0001 C CNN
F 3 "" H 2600 6800 50  0001 C CNN
	1    2600 6800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR017
U 1 1 5B987077
P 2600 7100
F 0 "#PWR017" H 2600 6850 50  0001 C CNN
F 1 "GND" H 2605 6927 50  0000 C CNN
F 2 "" H 2600 7100 50  0001 C CNN
F 3 "" H 2600 7100 50  0001 C CNN
	1    2600 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 7100 2600 6950
$Comp
L power:GND #PWR020
U 1 1 5B98707E
P 3500 7150
F 0 "#PWR020" H 3500 6900 50  0001 C CNN
F 1 "GND" H 3505 6977 50  0000 C CNN
F 2 "" H 3500 7150 50  0001 C CNN
F 3 "" H 3500 7150 50  0001 C CNN
	1    3500 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 7000 3500 7150
$Comp
L Device:C C?
U 1 1 5B987085
P 4650 6650
AR Path="/5B7CD4BD/5B987085" Ref="C?"  Part="1" 
AR Path="/5B7CD50C/5B987085" Ref="C26"  Part="1" 
F 0 "C26" H 4750 6650 50  0000 L CNN
F 1 "0.1uF" H 4700 6550 50  0000 L CNN
F 2 "modules:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4688 6500 50  0001 C CNN
F 3 "~" H 4650 6650 50  0001 C CNN
	1    4650 6650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR023
U 1 1 5B98708C
P 4650 7000
F 0 "#PWR023" H 4650 6750 50  0001 C CNN
F 1 "GND" H 4655 6827 50  0000 C CNN
F 2 "" H 4650 7000 50  0001 C CNN
F 3 "" H 4650 7000 50  0001 C CNN
	1    4650 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 6800 4650 7000
Wire Wire Line
	4650 6500 4650 6350
Wire Wire Line
	5050 6350 5450 6350
Wire Wire Line
	2100 6500 2600 6500
Wire Wire Line
	2600 6650 2600 6500
Connection ~ 2600 6500
Wire Wire Line
	2600 6500 2800 6500
$Comp
L Device:C C?
U 1 1 5B9870A1
P 5050 6650
AR Path="/5B7CD4BD/5B9870A1" Ref="C?"  Part="1" 
AR Path="/5B7CD50C/5B9870A1" Ref="C29"  Part="1" 
F 0 "C29" H 5150 6650 50  0000 L CNN
F 1 "33uF" H 5100 6550 50  0000 L CNN
F 2 "modules:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5088 6500 50  0001 C CNN
F 3 "" H 5050 6650 50  0001 C CNN
	1    5050 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 6800 5050 7000
Wire Wire Line
	5050 6500 5050 6350
$Comp
L power:GND #PWR026
U 1 1 5B9870AA
P 5050 7000
F 0 "#PWR026" H 5050 6750 50  0001 C CNN
F 1 "GND" H 5055 6827 50  0000 C CNN
F 2 "" H 5050 7000 50  0001 C CNN
F 3 "" H 5050 7000 50  0001 C CNN
	1    5050 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 6350 5050 6350
Connection ~ 4650 6350
Connection ~ 5050 6350
Wire Wire Line
	2750 8750 2950 8750
Wire Wire Line
	2750 8900 2750 8750
Connection ~ 2750 8750
Wire Wire Line
	2250 8750 2750 8750
Wire Wire Line
	3650 9250 3650 9400
$Comp
L power:GND #PWR021
U 1 1 5B9758C2
P 3650 9400
F 0 "#PWR021" H 3650 9150 50  0001 C CNN
F 1 "GND" H 3655 9227 50  0000 C CNN
F 2 "" H 3650 9400 50  0001 C CNN
F 3 "" H 3650 9400 50  0001 C CNN
	1    3650 9400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 9350 2750 9200
$Comp
L power:GND #PWR018
U 1 1 5B9758BB
P 2750 9350
F 0 "#PWR018" H 2750 9100 50  0001 C CNN
F 1 "GND" H 2755 9177 50  0000 C CNN
F 2 "" H 2750 9350 50  0001 C CNN
F 3 "" H 2750 9350 50  0001 C CNN
	1    2750 9350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5B9758B4
P 2750 9050
AR Path="/5B7CD4BD/5B9758B4" Ref="C?"  Part="1" 
AR Path="/5B7CD50C/5B9758B4" Ref="C24"  Part="1" 
F 0 "C24" H 2850 9050 50  0000 L CNN
F 1 "10uF" H 2800 8950 50  0000 L CNN
F 2 "modules:C_0805_HandSoldering" H 2788 8900 50  0001 C CNN
F 3 "" H 2750 9050 50  0001 C CNN
	1    2750 9050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5B987D0E
P 4800 8900
AR Path="/5B7CD4BD/5B987D0E" Ref="C?"  Part="1" 
AR Path="/5B7CD50C/5B987D0E" Ref="C27"  Part="1" 
F 0 "C27" H 4900 8900 50  0000 L CNN
F 1 "0.1uF" H 4850 8800 50  0000 L CNN
F 2 "modules:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4838 8750 50  0001 C CNN
F 3 "~" H 4800 8900 50  0001 C CNN
	1    4800 8900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR024
U 1 1 5B987D15
P 4800 9250
F 0 "#PWR024" H 4800 9000 50  0001 C CNN
F 1 "GND" H 4805 9077 50  0000 C CNN
F 2 "" H 4800 9250 50  0001 C CNN
F 3 "" H 4800 9250 50  0001 C CNN
	1    4800 9250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 9050 4800 9250
Wire Wire Line
	4800 8750 4800 8600
Wire Wire Line
	5200 8600 5550 8600
$Comp
L Device:C C?
U 1 1 5B987D1F
P 5200 8900
AR Path="/5B7CD4BD/5B987D1F" Ref="C?"  Part="1" 
AR Path="/5B7CD50C/5B987D1F" Ref="C30"  Part="1" 
F 0 "C30" H 5300 8900 50  0000 L CNN
F 1 "33uF" H 5250 8800 50  0000 L CNN
F 2 "modules:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5238 8750 50  0001 C CNN
F 3 "" H 5200 8900 50  0001 C CNN
	1    5200 8900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 9050 5200 9250
Wire Wire Line
	5200 8750 5200 8600
$Comp
L power:GND #PWR027
U 1 1 5B987D28
P 5200 9250
F 0 "#PWR027" H 5200 9000 50  0001 C CNN
F 1 "GND" H 5205 9077 50  0000 C CNN
F 2 "" H 5200 9250 50  0001 C CNN
F 3 "" H 5200 9250 50  0001 C CNN
	1    5200 9250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 8600 5200 8600
Connection ~ 5200 8600
$Comp
L ZLDO1117G33TA:ZLDO1117G33TA U6
U 1 1 5BA43072
P 3650 4200
F 0 "U6" H 3650 4687 60  0000 C CNN
F 1 "ZLDO1117G33TA" H 3650 4581 60  0000 C CNN
F 2 "modules:SOT-223" H 3650 4581 60  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/ZLDO1117.pdf" H 3650 4581 60  0001 C CNN
F 4 "ZLDO1117G33DICT-ND" H 3650 4200 50  0001 C CNN "DigiKey"
	1    3650 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 4050 4450 4050
Wire Wire Line
	4350 4350 4450 4350
Wire Wire Line
	4450 4350 4450 4050
Connection ~ 4450 4050
Wire Wire Line
	4450 4050 4800 4050
$Comp
L ZLDO1117G33TA:ZLDO1117G33TA U4
U 1 1 5BA446A0
P 3500 6500
F 0 "U4" H 3500 6987 60  0000 C CNN
F 1 "ZLDO1117G33TA" H 3500 6881 60  0000 C CNN
F 2 "modules:SOT-223" H 3500 6881 60  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/ZLDO1117.pdf" H 3500 6881 60  0001 C CNN
F 4 "ZLDO1117G33DICT-ND" H 3500 6500 50  0001 C CNN "DigiKey"
	1    3500 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 6350 4350 6350
Wire Wire Line
	4200 6650 4350 6650
Wire Wire Line
	4350 6650 4350 6350
Connection ~ 4350 6350
Wire Wire Line
	4350 6350 4650 6350
$Comp
L ZLDO1117G33TA:ZLDO1117G33TA U5
U 1 1 5BA4653F
P 3650 8750
F 0 "U5" H 3650 9237 60  0000 C CNN
F 1 "ZLDO1117G12TA" H 3650 9131 60  0000 C CNN
F 2 "modules:SOT-223" H 3800 8800 60  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/ZLDO1117.pdf" H 3800 8800 60  0001 C CNN
F 4 "ZLDO1117G12DICT-ND" H 3650 8750 50  0001 C CNN "DigiKey"
	1    3650 8750
	1    0    0    -1  
$EndComp
Connection ~ 4800 8600
Wire Wire Line
	4350 8600 4500 8600
Wire Wire Line
	4350 8900 4500 8900
Wire Wire Line
	4500 8900 4500 8600
Connection ~ 4500 8600
Wire Wire Line
	4500 8600 4800 8600
Wire Notes Line
	450  3000 7800 3000
$Comp
L Device:C C25
U 1 1 5BA4EB3D
P 2750 4500
F 0 "C25" H 2865 4546 50  0000 L CNN
F 1 "33uF" H 2865 4455 50  0000 L CNN
F 2 "modules:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2788 4350 50  0001 C CNN
F 3 "" H 2750 4500 50  0001 C CNN
	1    2750 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C31
U 1 1 5BA4F097
P 5200 4350
F 0 "C31" H 5315 4396 50  0000 L CNN
F 1 "33uF" H 5315 4305 50  0000 L CNN
F 2 "modules:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5238 4200 50  0001 C CNN
F 3 "" H 5200 4350 50  0001 C CNN
	1    5200 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5BA6520D
P 3150 2250
AR Path="/5B7CD4F6/5BA6520D" Ref="#PWR?"  Part="1" 
AR Path="/5B7CD50C/5BA6520D" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 3150 2000 50  0001 C CNN
F 1 "GND" H 3155 2077 50  0000 C CNN
F 2 "" H 3150 2250 50  0001 C CNN
F 3 "" H 3150 2250 50  0001 C CNN
	1    3150 2250
	1    0    0    -1  
$EndComp
Text Notes 3750 1500 0    59   ~ 12
MICRO USB CONECTOR
$Comp
L Device:C C?
U 1 1 5BA6522E
P 3700 2200
AR Path="/5B7CD4F6/5BA6522E" Ref="C?"  Part="1" 
AR Path="/5B7CD50C/5BA6522E" Ref="C62"  Part="1" 
F 0 "C62" H 3500 2400 50  0000 L CNN
F 1 "0.1uF" H 3450 2300 50  0000 L CNN
F 2 "modules:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3738 2050 50  0001 C CNN
F 3 "~" H 3700 2200 50  0001 C CNN
	1    3700 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5BA65235
P 3400 2450
AR Path="/5B7CD4F6/5BA65235" Ref="#PWR?"  Part="1" 
AR Path="/5B7CD50C/5BA65235" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 3400 2200 50  0001 C CNN
F 1 "GND" H 3405 2277 50  0000 C CNN
F 2 "" H 3400 2450 50  0001 C CNN
F 3 "" H 3400 2450 50  0001 C CNN
	1    3400 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 2050 3400 1900
Wire Wire Line
	3700 2050 3700 1900
Connection ~ 3700 1900
Wire Wire Line
	3700 1900 3400 1900
Wire Wire Line
	3700 2350 3700 2450
Wire Wire Line
	3700 2450 3400 2450
Wire Wire Line
	3400 2350 3400 2450
Connection ~ 3400 2450
$Comp
L Device:C C?
U 1 1 5BA65268
P 3400 2200
AR Path="/5B7CD4F6/5BA65268" Ref="C?"  Part="1" 
AR Path="/5B7CD50C/5BA65268" Ref="C61"  Part="1" 
F 0 "C61" H 3200 2400 50  0000 L CNN
F 1 "220uF" H 3150 2300 50  0000 L CNN
F 2 "modules:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3438 2050 50  0001 C CNN
F 3 "~" H 3400 2200 50  0001 C CNN
	1    3400 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 1900 3400 1900
Connection ~ 3400 1900
Wire Wire Line
	3700 1900 4050 1900
Text GLabel 4050 1900 2    50   Input ~ 0
+5V
Text GLabel 2250 4200 0    50   Input ~ 0
+5V
Text GLabel 2100 6500 0    50   Input ~ 0
+5V
$Comp
L Device:C C?
U 1 1 5BA7FE5F
P 4050 2200
AR Path="/5B7CD4F6/5BA7FE5F" Ref="C?"  Part="1" 
AR Path="/5B7CD50C/5BA7FE5F" Ref="C65"  Part="1" 
F 0 "C65" H 3900 2400 50  0000 L CNN
F 1 "1nF" H 3900 2300 50  0000 L CNN
F 2 "modules:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4088 2050 50  0001 C CNN
F 3 "~" H 4050 2200 50  0001 C CNN
	1    4050 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 2050 4050 1900
Wire Wire Line
	4050 2350 4050 2450
Wire Wire Line
	4050 2450 3700 2450
Connection ~ 3700 2450
$Comp
L Connector:Barrel_Jack_Switch J4
U 1 1 5BB2AB3A
P 2750 2000
F 0 "J4" H 2805 2317 50  0000 C CNN
F 1 "Barrel_Jack_Switch" H 2805 2226 50  0000 C CNN
F 2 "modules:BARREL_JACK" H 2800 1960 50  0001 C CNN
F 3 "https://www.cui.com/product/resource/pj-002a.pdf" H 2800 1960 50  0001 C CNN
F 4 "CP-002A-ND" H 2750 2000 50  0001 C CNN "DigiKey"
	1    2750 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 2000 3150 2000
Wire Wire Line
	3150 2000 3150 2100
Wire Wire Line
	3150 2100 3050 2100
Wire Wire Line
	3150 2250 3150 2100
Connection ~ 3150 2100
$Comp
L BAT2032:LED D9
U 1 1 5C8F23F1
P 5700 2200
F 0 "D9" V 5738 2083 50  0000 R CNN
F 1 "LED" V 5647 2083 50  0000 R CNN
F 2 "modules:LED_0805_HandSoldering" H 5700 2200 50  0001 C CNN
F 3 "~" H 5700 2200 50  0001 C CNN
	1    5700 2200
	0    -1   -1   0   
$EndComp
$Comp
L BAT2032:R R56
U 1 1 5C91E805
P 5700 2500
F 0 "R56" H 5770 2546 50  0000 L CNN
F 1 "2k" H 5770 2455 50  0000 L CNN
F 2 "modules:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5630 2500 50  0001 C CNN
F 3 "~" H 5700 2500 50  0001 C CNN
	1    5700 2500
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR079
U 1 1 5C91E97D
P 5700 2050
F 0 "#PWR079" H 5700 1900 50  0001 C CNN
F 1 "+5V" H 5715 2223 50  0000 C CNN
F 2 "" H 5700 2050 50  0001 C CNN
F 3 "" H 5700 2050 50  0001 C CNN
	1    5700 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR080
U 1 1 5C91EA14
P 5700 2650
F 0 "#PWR080" H 5700 2400 50  0001 C CNN
F 1 "GND" H 5705 2477 50  0000 C CNN
F 2 "" H 5700 2650 50  0001 C CNN
F 3 "" H 5700 2650 50  0001 C CNN
	1    5700 2650
	1    0    0    -1  
$EndComp
Text Notes 5150 2450 0    50   ~ 0
Indicador de\nEncendido
Wire Notes Line
	5050 2950 5050 1800
Wire Notes Line
	5050 1800 6000 1800
Wire Notes Line
	6000 1800 6000 2950
Wire Notes Line
	6000 2950 5050 2950
$Comp
L BAT2032:R R57
U 1 1 5C8F1C9D
P 5550 8950
F 0 "R57" H 5620 8996 50  0000 L CNN
F 1 "120" H 5620 8905 50  0000 L CNN
F 2 "modules:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5480 8950 50  0001 C CNN
F 3 "~" H 5550 8950 50  0001 C CNN
	1    5550 8950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR081
U 1 1 5C8F1D98
P 5550 9250
F 0 "#PWR081" H 5550 9000 50  0001 C CNN
F 1 "GND" H 5555 9077 50  0000 C CNN
F 2 "" H 5550 9250 50  0001 C CNN
F 3 "" H 5550 9250 50  0001 C CNN
	1    5550 9250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 9250 5550 9100
Wire Wire Line
	5550 8800 5550 8600
Connection ~ 5550 8600
Wire Wire Line
	5550 8600 5600 8600
$EndSCHEMATC
