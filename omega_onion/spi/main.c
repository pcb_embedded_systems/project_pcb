#include <stdio.h>
#include <onion-spi.h>

///////////////////////////librerías usadas en iceprog /////////////////////////////////
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
///////////////////////////librerías usadas en iceprog /////////////////////////////////

#include "gpio.h"


#define RESET_FPGA 1
#define CS_FPGA 6
#define N_BYTES 256 //cantidad de bytes a enviar

static bool verbose = false;

void readValue(int addr, struct spiParams *params)
{
	int 		status, size;
	uint8_t 	*txBuffer;
	uint8_t 	*rxBuffer;

	//struct spiParams	params;
	//spiParamInit(&params);

	// set the transmission size and allocate memory for the buffers
	size 		= 4;
	txBuffer	= (uint8_t*)malloc(sizeof(uint8_t) * size);
	rxBuffer	= (uint8_t*)malloc(sizeof(uint8_t) * size);

	// assign the register address to the transmission buffer
	*txBuffer 	= (uint8_t)addr;

	// invoke the SPI transfer
	status 	= spiTransfer(params, txBuffer, rxBuffer, size);

	// rxBuffer now contains the data read through the SPI interface
	printf("> SPI Read from addr 0x%02x: 0x%02x\n", addr, *rxBuffer);

	// clean-up
	free(txBuffer);
	free(rxBuffer);
}


void writeValue(int addr, int value, struct spiParams *params)
{
	int 		status, size;
	uint8_t 	*txBuffer;
	uint8_t 	*rxBuffer;

	//struct spiParams	params;
	//spiParamInit(&params);

	// set the transmission size and allocate memory for the buffers
	size 		= 1;
	txBuffer	= (uint8_t*)malloc(sizeof(uint8_t) * size);
	rxBuffer 	= (uint8_t*)malloc(sizeof(uint8_t) * size);

	// assign the register address and data to be written to the transmission buffer
	txBuffer[0] = (uint8_t)addr;
	txBuffer[1] = (uint8_t)value;

	// invoke the SPI transfer
	status 	= spiTransfer(params, txBuffer, rxBuffer, size);

	// data has been written
	// any response is now in rxBuffer
	if(verbose)
		printf("> SPI Write to addr 0x%02x: 0x%02x\n", txBuffer[0], txBuffer[1] );

	// clean-up
	free(txBuffer);
	free(rxBuffer);
}


void readValue2(int addr, struct spiParams *params)
{
	int 		status, size;
	uint8_t 	*rdBuffer;

	//struct spiParams	params;
	//spiParamInit(&params);

	// set the transmission size and allocate memory for the buffers
	size 		= 3;
	rdBuffer	= (uint8_t*)malloc(sizeof(uint8_t) * size);

	// invoke the SPI read
	status 		= spiRead(params, addr, rdBuffer, size);

	// rdBuffer now contains the data read through the SPI interface
	if (verbose)
		printf("> SPI Read from addr 0x%02x: 0x%02x\n", addr, *rdBuffer);

	// clean-up
	free(rdBuffer);
}


void writeValue2(int addr, int value, struct spiParams *params)
{
	int 		status, size,i;
	uint8_t 	*wrBuffer;

//	struct spiParams	params;
//	spiParamInit(&params);

	// set the transmission size and allocate memory for the buffers
	size 		= 5;
	wrBuffer	= (uint8_t*)malloc(sizeof(uint8_t) * size);
	for (i=0;i<size;i++)
	{
		wrBuffer[i]=0;
	}

	// put the value to be written in the wrBuffer
	*wrBuffer 	= (uint8_t)value;

	// invoke the SPI read
	status 		= spiWrite(params, addr, wrBuffer, size);

	// data in wrBuffer has been written to the SPI device
	if(verbose)
		printf("> SPI Write to addr 0x%02x: 0x%02x\n", addr, *wrBuffer );

	// clean-up
	free(wrBuffer);
}


int writeByte (uint8_t *addr, int value, struct spiParams *params)
{
	int 		status, i;
	uint8_t 	*txBuffer;
	uint8_t 	*rxBuffer;

	// generate the transmission buffer
	txBuffer	= (uint8_t*)malloc(sizeof(uint8_t) * (5) );
	rxBuffer 	= (uint8_t*)malloc(sizeof(uint8_t) * (1) );

	// load the address
	txBuffer[0]=0x02;//opcode para escribir
	for(i=1;i<4;i++){
	txBuffer[i]=(uint8_t)addr[i-1];
	}

	// load the data to write
	txBuffer[4] 	= (uint8_t)value;

	// call the transfer
	status 	= spiTransfer(params, txBuffer, rxBuffer, sizeof(txBuffer)+1);

	// clean-up
	free(txBuffer);
	free(rxBuffer);

	return 	status;
}

int readByte (uint8_t *addr, uint8_t *rdBuffer, struct spiParams *params)
{
	int 		status, i;
	uint8_t 	*txBuffer;
	uint8_t 	*rxBuffer;

	// generate the transmission buffer
	txBuffer	= (uint8_t*)malloc(sizeof(uint8_t) * 10 );

	txBuffer[0]=0x03;//opcode para leer
	// load the address
	for(i=1;i<4;i++){
	txBuffer[i]=(uint8_t)addr[i-1];
	}
	txBuffer[4]=0;
	txBuffer[5]=0;
	txBuffer[6]=0;
	txBuffer[7]=0;
	txBuffer[8]=0;
	txBuffer[9]=0;

	// call the transfer
	status 	= spiTransfer(params, txBuffer, rdBuffer, 10);

	// clean-up
	free(txBuffer);
	free(rxBuffer);

	return 	status;
}

int spi_Init(struct spiParams *params)
{
  int     status;
  // Inicializar los parámetros de spi
  spiParamInit(params);
  params->busNum   = 1;
  params->deviceId = 32766;
  params->csGpio   = 6;
  params->sckGpio  = 7;
  params->mosiGpio = 8;
  params->misoGpio = 9;
  params->speedInHz = 2000000;

  //verificar si el dispositivo spi esta registrado
  status  = spiCheckDevice(params->busNum, params->deviceId, ONION_SEVERITY_DEBUG_EXTRA);
  if (status == EXIT_SUCCESS) {
      printf("SPI Device is mapped.\n");
  }
  else {
    printf("WARNING: SPI Device NOT mapped!\n");
    return status;
  }

  // Registrar dispositivo
  status  = spiRegisterDevice(params);
  if (status == EXIT_SUCCESS) {
    printf("Dispositivo SPI registrado.\n");
  }
  else {
    printf("WARNING: Dispositivo SPI no registrado!\n");
  return status;
  }
}

int reset_fpga()
{
 gpio_open(RESET_FPGA);
 gpio_set_direction(RESET_FPGA, "out");
 gpio_set(RESET_FPGA, 0);
 sleep (1);
 gpio_close(RESET_FPGA);
}

int unreset_fpga()
{
 gpio_open(RESET_FPGA);
 gpio_set_direction(RESET_FPGA, "out");
 gpio_set(RESET_FPGA, 1);
 sleep (1);
 gpio_close(RESET_FPGA);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static void flash_chip_select()
{
gpio_open(CS_FPGA);
gpio_set_direction(CS_FPGA, "out");
gpio_set(CS_FPGA, 0);
gpio_close(CS_FPGA);
}

// FLASH chip select deassert
static void flash_chip_deselect()
{
gpio_open(CS_FPGA);
gpio_set_direction(CS_FPGA, "out");
gpio_set(CS_FPGA, 1);
gpio_close(CS_FPGA);
}

static void send_spi(int addr, uint8_t *data, int n, struct spiParams *params)
{
	int 		status, i;
	uint8_t 	*txBuffer;
	uint8_t 	*rxBuffer;
	uint8_t command[4] = { 0x02, (uint8_t)(addr >> 16), (uint8_t)(addr >> 8), (uint8_t)addr };
	// generate the transmission buffer
	txBuffer	= (uint8_t*)malloc(sizeof(uint8_t) * (n+4) );
	rxBuffer 	= (uint8_t*)malloc(sizeof(uint8_t) * (n+4) );
	// load the address

	for (i=0;i<4;i++)
	{
	  txBuffer[i] = command[i];
	}
	// load the data to write
	for (i=0;i<n;i++)
	{
	  txBuffer[i+4] = data[i];
	}
	// call the transfer
	status 	= spiTransfer(params, txBuffer, rxBuffer, n+4);

	if (verbose)
		for (int i = 0; i < n; i++)
			fprintf(stderr, "%02x%c", txBuffer[i], i == n - 1 || i % 32 == 31 ? '\n' : ' ');
	// clean-up
	free(txBuffer);
	free(rxBuffer);
}

static void flash_prog(int addr, uint8_t *data, int n, struct spiParams *params)
{
	if (verbose)
		fprintf(stderr, "prog 0x%06X +0x%03X..\n", addr, n);

	uint8_t command[4] = { 0x02, (uint8_t)(addr >> 16), (uint8_t)(addr >> 8), (uint8_t)addr };

//	flash_chip_select();
//	send_spi(command, 4, params);
	send_spi(addr, data, n, params);
//	flash_chip_deselect();

	if (verbose)
		for (int i = 0; i < n; i++)
			fprintf(stderr, "%02x%c", data[i], i == n - 1 || i % 32 == 31 ? '\n' : ' ');
}


static void flash_write_enable(struct spiParams *params)
{
	if (verbose)
		fprintf(stderr, "write enable..\n");
	writeValue(0x06,0x00,params);//write enable
}

static void flash_chip_erase(struct spiParams *params)
{
//Chip Erase (60h or C7h)
//blok erase (52h ---> 32Kb)
	int 		status, i;
	uint8_t 	*txBuffer;
	uint8_t 	*rxBuffer;
	int 		addr =0;
	uint8_t command[4] = { 0x52, (uint8_t)(addr >> 16), (uint8_t)(addr >> 8), (uint8_t)addr };

	if (verbose)
		fprintf(stderr, "block erase..\n");

	// generate the transmission buffer
	txBuffer	= (uint8_t*)malloc(sizeof(uint8_t) * (4) );
	rxBuffer 	= (uint8_t*)malloc(sizeof(uint8_t) * (4) );
	// load the address
	for (i=0;i<4;i++)
	  txBuffer[i] = command[i];
	// call the transfer
	status 	= spiTransfer(params, txBuffer, rxBuffer, 4);
	// clean-up
	free(txBuffer);
	free(rxBuffer);
	sleep(2);//tiempo para esperar que el bloque de memoria se borre

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
  struct spiParams params;
  int status = spi_Init(&params);//función que inicializa y registra el dispositivo SPI
  if (status == EXIT_SUCCESS) {}
  else {return 0;}

uint8_t addr_[3];
addr_[0]=0x00;
addr_[1]=0x00;
addr_[2]=0x00;
uint8_t read_buffer[10] = {0,0,0,0,0,0,0,0,0,0};
int i;
  reset_fpga();
//  writeValue2(0xB9,0x00,&params);//deep power down
//  printf("flash power down\n");
  sleep (2);
  writeValue2(0xAB,0x00,&params);//resume deep power down
  flash_write_enable(&params);
  flash_chip_erase(&params);
  sleep(5);//flash_wait;
//  printf("flash resume from power down\n");
//  sleep (5);
//  writeValue2(0x90,0x00,&params);//read ID 1F15
//  writeValue(0x06,0x00,&params);//write enable
//  sleep(5);
//  writeByte(&addr[0],0x55,&params);//program
//  sleep(5);
//  readByte(&addr[0],&read_buffer[0],&params);//read
//  int i=0;
//  for (i=0;i<6;i++)
//  {
//  printf("El dato leido de la memoria es 0x%02x\n",read_buffer[i]);
//  }

//  printf ("fin\n");

//////////////////////////////leer archivo como en iceprog/////////////////////////////////////////////

const char *filename = NULL;
const char *my_name = argv[0];
int rw_offset = 0;

if (optind + 1 == argc) {
	filename = argv[optind];
	}

FILE *f = NULL;
long file_size = -1;

f = (strcmp(filename, "-") == 0) ? stdin : fopen(filename, "rb");
if (filename==NULL)
 return EXIT_FAILURE;
printf("abre el archivo y le da el indicador a f\n");
	if (f == NULL) {
		fprintf(stderr, "%s: can't open '%s' for reading: ", my_name, filename);
		perror(0);
		return EXIT_FAILURE;
	}

fprintf(stderr, "programming..\n");
	for (int rc, addr = 0; true; addr += rc) {
		uint8_t buffer[N_BYTES];
		int page_size = N_BYTES - (rw_offset + addr) % N_BYTES;
		rc = fread(buffer, 1, page_size, f);
		if (rc <= 0)
			break;
		flash_write_enable(&params);
//		flash_prog(rw_offset + addr, buffer, rc, &params);
		send_spi(rw_offset + addr, &buffer[0], rc, &params);
		usleep(20000);//flash_wait();
		if (verbose)
			printf("rc: %u, addr: %02x, dato [rc]: %02x %02x %02x %02x \n",rc,addr,buffer[0],buffer[1],buffer[2],buffer[3]);
				}
printf("Done\n");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////*/


/*/////////////////////////////////////////////////////////////////////////////
	sleep(5);
	uint8_t buffer_[] = {0xff,0x00,0x00,0xff,0x7e,0xaa,0x99,0x7e};
	flash_write_enable(&params);
	flash_chip_erase(&params);
	sleep(5);//flash_wait;
	flash_write_enable(&params);
	send_spi(0, &buffer_[0], 8, &params);
/////////////////////////////////////////////////////////////////////////////*/








/*//////////////////////////////////////////////////////////////////////////////
sleep(1);
readByte(&addr_[0],&read_buffer[0],&params);//read
printf("\n");
for (i=0;i<10;i++){
printf("[%d] = %02x ",i,read_buffer[i]);
}

addr_[0]=0x00;
addr_[1]=0x00;
addr_[2]=0x01;
sleep(1);
readByte(&addr_[0],&read_buffer[0],&params);//read
printf("\n");
for (i=0;i<10;i++){
printf("[%d] = %02x ",i,read_buffer[i]);
}


addr_[0]=0x00;
addr_[1]=0x00;
addr_[2]=0x02;
sleep(1);
readByte(&addr_[0],&read_buffer[0],&params);//read
printf("\n");
for (i=0;i<10;i++){
printf("[%d] = %02x ",i,read_buffer[i]);
}


addr_[0]=0x00;
addr_[1]=0x00;
addr_[2]=0x03;
sleep(1);
readByte(&addr_[0],&read_buffer[0],&params);//read
printf("\n");
for (i=0;i<10;i++){
printf("[%d] = %02x ",i,read_buffer[i]);
}


addr_[0]=0x00;
addr_[1]=0x00;
addr_[2]=0x04;
sleep(1);
readByte(&addr_[0],&read_buffer[0],&params);//read
printf("\n");
for (i=0;i<10;i++){
printf("[%d] = %02x ",i,read_buffer[i]);
}


addr_[0]=0x00;
addr_[1]=0x00;
addr_[2]=0x05;
sleep(1);
readByte(&addr_[0],&read_buffer[0],&params);//read
printf("\n");
for (i=0;i<10;i++){
printf("[%d] = %02x ",i,read_buffer[i]);
}


addr_[0]=0x00;
addr_[1]=0x00;
addr_[2]=0x06;
sleep(1);
readByte(&addr_[0],&read_buffer[0],&params);//read
printf("\n");
for (i=0;i<10;i++){
printf("[%d] = %02x ",i,read_buffer[i]);
}



addr_[0]=0x00;
addr_[1]=0x00;
addr_[2]=0x07;
sleep(1);
readByte(&addr_[0],&read_buffer[0],&params);//read
printf("\n");
for (i=0;i<10;i++){
printf("[%d] = %02x ",i,read_buffer[i]);
}
printf("\n");

//////////////////////////////////////////////////////////////////////////////////////////*/

  unreset_fpga();
  reset_fpga();
  unreset_fpga();
//////////////////////////////leer archivo como en iceprog/////////////////////////////////////////////

  return 0;
}
