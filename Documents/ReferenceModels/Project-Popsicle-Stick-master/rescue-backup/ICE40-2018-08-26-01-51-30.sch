EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:STM32F07
LIBS:st_regul
LIBS:w_microcontrollers
LIBS:popsiclestick-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title "Project Popsicle Stick"
Date "2016-01-24"
Rev "1.0"
Comp ""
Comment1 "Author: Brosnan Yuen"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ICE40HX1K-TQ144 U4
U 1 1 56A5401F
P 5150 3400
F 0 "U4" H 5150 3450 60  0000 C CNN
F 1 "ICE40HX1K-TQ144" H 5150 3350 60  0000 C CNN
F 2 "Housings_QFP:TQFP-144_20x20mm_Pitch0.5mm" H 5150 3550 60  0001 C CNN
F 3 "" H 5150 3550 60  0000 C CNN
	1    5150 3400
	1    0    0    -1  
$EndComp
Text HLabel 1500 700  0    60   Input ~ 0
P1V2
Text HLabel 4450 5500 0    60   Input ~ 0
PGND
NoConn ~ 6550 4650
NoConn ~ 6550 4700
NoConn ~ 6550 4750
NoConn ~ 6550 4800
NoConn ~ 6550 4850
NoConn ~ 6550 4900
NoConn ~ 6550 4950
NoConn ~ 6550 5000
NoConn ~ 3750 4450
NoConn ~ 3750 4500
NoConn ~ 3750 4550
NoConn ~ 3750 4600
NoConn ~ 3750 4650
NoConn ~ 3750 4700
NoConn ~ 3750 4750
NoConn ~ 3750 4800
NoConn ~ 3750 4850
NoConn ~ 3750 4900
NoConn ~ 3750 4950
NoConn ~ 3750 5000
Text HLabel 1500 1250 0    60   Input ~ 0
PGND
$Comp
L C C19
U 1 1 56A46909
P 1650 950
F 0 "C19" H 1675 1050 50  0000 L CNN
F 1 "10uF" H 1675 850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 1688 800 50  0001 C CNN
F 3 "" H 1650 950 50  0000 C CNN
	1    1650 950 
	1    0    0    -1  
$EndComp
$Comp
L C C21
U 1 1 56A4692E
P 1900 950
F 0 "C21" H 1925 1050 50  0000 L CNN
F 1 "1uF" H 1925 850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 1938 800 50  0001 C CNN
F 3 "" H 1900 950 50  0000 C CNN
	1    1900 950 
	1    0    0    -1  
$EndComp
$Comp
L C C23
U 1 1 56A4694D
P 2150 950
F 0 "C23" H 2175 1050 50  0000 L CNN
F 1 "0.1uF" H 2175 850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 2188 800 50  0001 C CNN
F 3 "" H 2150 950 50  0000 C CNN
	1    2150 950 
	1    0    0    -1  
$EndComp
$Comp
L C C25
U 1 1 56A46BFD
P 2400 950
F 0 "C25" H 2425 1050 50  0000 L CNN
F 1 "0.1uF" H 2425 850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 2438 800 50  0001 C CNN
F 3 "" H 2400 950 50  0000 C CNN
	1    2400 950 
	1    0    0    -1  
$EndComp
$Comp
L C C27
U 1 1 56A46C22
P 2650 950
F 0 "C27" H 2675 1050 50  0000 L CNN
F 1 "0.1uF" H 2675 850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 2688 800 50  0001 C CNN
F 3 "" H 2650 950 50  0000 C CNN
	1    2650 950 
	1    0    0    -1  
$EndComp
$Comp
L C C29
U 1 1 56A46C52
P 2900 950
F 0 "C29" H 2925 1050 50  0000 L CNN
F 1 "0.1uF" H 2925 850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 2938 800 50  0001 C CNN
F 3 "" H 2900 950 50  0000 C CNN
	1    2900 950 
	1    0    0    -1  
$EndComp
$Comp
L C C31
U 1 1 56A46C7D
P 3150 950
F 0 "C31" H 3175 1050 50  0000 L CNN
F 1 "0.011uF" H 3175 850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3188 800 50  0001 C CNN
F 3 "" H 3150 950 50  0000 C CNN
	1    3150 950 
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 56A47921
P 5750 1050
F 0 "R4" V 5830 1050 50  0000 C CNN
F 1 "100Ω" V 5750 1050 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5680 1050 50  0001 C CNN
F 3 "" H 5750 1050 50  0000 C CNN
	1    5750 1050
	1    0    0    -1  
$EndComp
Text Label 5850 1250 3    60   ~ 0
PLLVCC
Text Label 6400 5150 2    60   ~ 0
PLLVCC
$Comp
L C C35
U 1 1 56A4A1E4
P 5750 5350
F 0 "C35" H 5775 5450 50  0000 L CNN
F 1 "10uF" H 5775 5250 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 5788 5200 50  0001 C CNN
F 3 "" H 5750 5350 50  0000 C CNN
	1    5750 5350
	1    0    0    -1  
$EndComp
$Comp
L C C36
U 1 1 56A4A4EC
P 6000 5350
F 0 "C36" H 6025 5450 50  0000 L CNN
F 1 "0.1uF" H 6025 5250 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 6038 5200 50  0001 C CNN
F 3 "" H 6000 5350 50  0000 C CNN
	1    6000 5350
	1    0    0    -1  
$EndComp
Text HLabel 7000 4500 2    60   Input ~ 0
CRESET_B
Text HLabel 7550 4450 2    60   Output ~ 0
CDONE
Text HLabel 7750 2950 2    60   Input ~ 0
SPI_SS_B
$Comp
L R R6
U 1 1 56A55881
P 7400 3150
F 0 "R6" V 7480 3150 50  0000 C CNN
F 1 "10KΩ" V 7400 3150 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 7330 3150 50  0001 C CNN
F 3 "" H 7400 3150 50  0000 C CNN
	1    7400 3150
	1    0    0    -1  
$EndComp
Text HLabel 7650 3350 2    60   Input ~ 0
PGND
Text HLabel 8300 2900 2    60   Input ~ 0
SPI_SCK
Text HLabel 8750 2850 2    60   Input ~ 0
SPI_SDI
Text HLabel 9150 2800 2    60   Output ~ 0
SPI_SDO
Text Label 5550 1300 3    60   ~ 0
SPIVCC
Text Label 8150 950  2    60   ~ 0
SPIVCC
Text HLabel 7200 1450 0    60   Input ~ 0
PGND
Text HLabel 7200 950  0    60   Input ~ 0
P3V3
$Comp
L C C37
U 1 1 56A58B67
P 7350 1200
F 0 "C37" H 7375 1300 50  0000 L CNN
F 1 "0.1uF" H 7375 1100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 7388 1050 50  0001 C CNN
F 3 "" H 7350 1200 50  0000 C CNN
	1    7350 1200
	1    0    0    -1  
$EndComp
Text HLabel 900  1350 0    60   Input ~ 0
P3V3
$Comp
L C C22
U 1 1 56A5956E
P 2100 1550
F 0 "C22" H 2125 1650 50  0000 L CNN
F 1 "0.1uF" H 2125 1450 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 2138 1400 50  0001 C CNN
F 3 "" H 2100 1550 50  0000 C CNN
	1    2100 1550
	1    0    0    -1  
$EndComp
Text HLabel 900  1750 0    60   Input ~ 0
PGND
Text HLabel 7550 3950 2    60   Input ~ 0
P3V3
$Comp
L R R7
U 1 1 56A5A909
P 7450 4200
F 0 "R7" V 7530 4200 50  0000 C CNN
F 1 "10KΩ" V 7450 4200 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 7380 4200 50  0001 C CNN
F 3 "" H 7450 4200 50  0000 C CNN
	1    7450 4200
	1    0    0    -1  
$EndComp
$Comp
L C C16
U 1 1 56A66F98
P 1050 1550
F 0 "C16" H 1075 1650 50  0000 L CNN
F 1 "10uF" H 1075 1450 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 1088 1400 50  0001 C CNN
F 3 "" H 1050 1550 50  0000 C CNN
	1    1050 1550
	1    0    0    -1  
$EndComp
$Comp
L C C17
U 1 1 56A66FF5
P 1300 1550
F 0 "C17" H 1325 1650 50  0000 L CNN
F 1 "1uF" H 1325 1450 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 1338 1400 50  0001 C CNN
F 3 "" H 1300 1550 50  0000 C CNN
	1    1300 1550
	1    0    0    -1  
$EndComp
$Comp
L C C18
U 1 1 56A67057
P 1550 1550
F 0 "C18" H 1575 1650 50  0000 L CNN
F 1 "0.1uF" H 1575 1450 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 1588 1400 50  0001 C CNN
F 3 "" H 1550 1550 50  0000 C CNN
	1    1550 1550
	1    0    0    -1  
$EndComp
$Comp
L C C20
U 1 1 56A670C2
P 1800 1550
F 0 "C20" H 1825 1650 50  0000 L CNN
F 1 "0.011uF" H 1825 1450 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 1838 1400 50  0001 C CNN
F 3 "" H 1800 1550 50  0000 C CNN
	1    1800 1550
	1    0    0    -1  
$EndComp
$Comp
L C C24
U 1 1 56A6850F
P 2350 1550
F 0 "C24" H 2375 1650 50  0000 L CNN
F 1 "0.1uF" H 2375 1450 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 2388 1400 50  0001 C CNN
F 3 "" H 2350 1550 50  0000 C CNN
	1    2350 1550
	1    0    0    -1  
$EndComp
$Comp
L C C26
U 1 1 56A686ED
P 2600 1550
F 0 "C26" H 2625 1650 50  0000 L CNN
F 1 "0.1uF" H 2625 1450 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 2638 1400 50  0001 C CNN
F 3 "" H 2600 1550 50  0000 C CNN
	1    2600 1550
	1    0    0    -1  
$EndComp
$Comp
L C C28
U 1 1 56A6874C
P 2850 1550
F 0 "C28" H 2875 1650 50  0000 L CNN
F 1 "0.1uF" H 2875 1450 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 2888 1400 50  0001 C CNN
F 3 "" H 2850 1550 50  0000 C CNN
	1    2850 1550
	1    0    0    -1  
$EndComp
$Comp
L C C30
U 1 1 56A687A8
P 3100 1550
F 0 "C30" H 3125 1650 50  0000 L CNN
F 1 "0.1uF" H 3125 1450 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 3138 1400 50  0001 C CNN
F 3 "" H 3100 1550 50  0000 C CNN
	1    3100 1550
	1    0    0    -1  
$EndComp
$Comp
L C C32
U 1 1 56A68803
P 3350 1550
F 0 "C32" H 3375 1650 50  0000 L CNN
F 1 "0.1uF" H 3375 1450 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 3388 1400 50  0001 C CNN
F 3 "" H 3350 1550 50  0000 C CNN
	1    3350 1550
	1    0    0    -1  
$EndComp
$Comp
L C C33
U 1 1 56A689B9
P 3600 1550
F 0 "C33" H 3625 1650 50  0000 L CNN
F 1 "0.1uF" H 3625 1450 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 3638 1400 50  0001 C CNN
F 3 "" H 3600 1550 50  0000 C CNN
	1    3600 1550
	1    0    0    -1  
$EndComp
$Comp
L C C34
U 1 1 56A68A1A
P 3850 1550
F 0 "C34" H 3875 1650 50  0000 L CNN
F 1 "0.1uF" H 3875 1450 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 3888 1400 50  0001 C CNN
F 3 "" H 3850 1550 50  0000 C CNN
	1    3850 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 5150 4600 5500
Wire Wire Line
	4450 5500 5400 5500
Wire Wire Line
	5400 5500 5400 5150
Connection ~ 4600 5500
Wire Wire Line
	5300 5150 5300 5500
Connection ~ 5300 5500
Wire Wire Line
	5200 5150 5200 5500
Connection ~ 5200 5500
Wire Wire Line
	5100 5150 5100 5500
Connection ~ 5100 5500
Wire Wire Line
	5000 5150 5000 5500
Connection ~ 5000 5500
Wire Wire Line
	4900 5150 4900 5500
Connection ~ 4900 5500
Wire Wire Line
	4800 5150 4800 5500
Connection ~ 4800 5500
Wire Wire Line
	4700 5150 4700 5500
Connection ~ 4700 5500
Wire Wire Line
	1500 700  5750 700 
Wire Wire Line
	5150 700  5150 1650
Wire Wire Line
	5300 700  5300 1650
Connection ~ 5150 700 
Wire Wire Line
	5250 1650 5250 700 
Connection ~ 5250 700 
Wire Wire Line
	5200 1650 5200 700 
Connection ~ 5200 700 
Wire Wire Line
	1500 1250 3150 1250
Wire Wire Line
	3150 1250 3150 1100
Wire Wire Line
	3150 800  3150 700 
Connection ~ 3150 700 
Wire Wire Line
	2900 700  2900 800 
Connection ~ 2900 700 
Wire Wire Line
	2650 800  2650 700 
Connection ~ 2650 700 
Wire Wire Line
	2400 800  2400 700 
Connection ~ 2400 700 
Wire Wire Line
	2150 800  2150 700 
Connection ~ 2150 700 
Wire Wire Line
	1900 800  1900 700 
Connection ~ 1900 700 
Wire Wire Line
	1650 600  1650 800 
Connection ~ 1650 700 
Wire Wire Line
	1650 1100 1650 1250
Connection ~ 1650 1250
Wire Wire Line
	2900 1100 2900 1250
Connection ~ 2900 1250
Wire Wire Line
	2650 1100 2650 1250
Connection ~ 2650 1250
Wire Wire Line
	2400 1100 2400 1250
Connection ~ 2400 1250
Wire Wire Line
	2150 1100 2150 1250
Connection ~ 2150 1250
Wire Wire Line
	1900 1100 1900 1250
Connection ~ 1900 1250
Wire Wire Line
	5750 700  5750 900 
Connection ~ 5300 700 
Wire Wire Line
	5750 1200 5750 1650
Wire Wire Line
	5750 1600 5850 1600
Wire Wire Line
	5850 1600 5850 1250
Connection ~ 5750 1600
Wire Wire Line
	5600 5150 5600 5550
Wire Wire Line
	6400 5150 5750 5150
Wire Wire Line
	5750 5150 5750 5200
Wire Wire Line
	6000 5200 6000 5150
Connection ~ 6000 5150
Wire Wire Line
	5600 5550 6000 5550
Wire Wire Line
	6000 5550 6000 5500
Wire Wire Line
	5750 5500 5750 5550
Connection ~ 5750 5550
Wire Wire Line
	6550 4500 7000 4500
Wire Wire Line
	6550 4450 7550 4450
Wire Wire Line
	6550 2950 7750 2950
Wire Wire Line
	7400 3000 7400 2950
Connection ~ 7400 2950
Wire Wire Line
	7400 3300 7400 3350
Wire Wire Line
	7400 3350 7650 3350
Wire Wire Line
	6550 2900 8300 2900
Wire Wire Line
	8750 2850 6550 2850
Wire Wire Line
	9150 2800 6550 2800
Wire Wire Line
	5550 1650 5550 1300
Wire Wire Line
	7200 950  8150 950 
Wire Wire Line
	7200 1450 7550 1450
Connection ~ 7350 950 
Wire Wire Line
	7050 3950 7550 3950
Wire Wire Line
	7450 3950 7450 4050
Wire Wire Line
	7450 4350 7450 4450
Connection ~ 7450 4450
Wire Wire Line
	900  1350 4950 1350
Wire Wire Line
	4950 1350 4950 1650
Wire Wire Line
	4900 1650 4900 1350
Connection ~ 4900 1350
Wire Wire Line
	4150 1650 4150 1350
Connection ~ 4150 1350
Wire Wire Line
	4200 1650 4200 1350
Connection ~ 4200 1350
Wire Wire Line
	4400 1650 4400 1350
Connection ~ 4400 1350
Wire Wire Line
	4450 1650 4450 1350
Connection ~ 4450 1350
Wire Wire Line
	4650 1650 4650 1350
Connection ~ 4650 1350
Wire Wire Line
	4700 1650 4700 1350
Connection ~ 4700 1350
Wire Wire Line
	900  1750 3750 1750
Wire Wire Line
	3750 1750 3750 1700
Wire Wire Line
	3750 1700 3850 1700
Wire Wire Line
	3600 1700 3600 1750
Connection ~ 3600 1750
Wire Wire Line
	3350 1700 3350 1750
Connection ~ 3350 1750
Wire Wire Line
	3100 1700 3100 1750
Connection ~ 3100 1750
Wire Wire Line
	2850 1700 2850 1750
Connection ~ 2850 1750
Wire Wire Line
	2600 1700 2600 1750
Connection ~ 2600 1750
Wire Wire Line
	2350 1700 2350 1750
Connection ~ 2350 1750
Wire Wire Line
	1050 1400 1050 1350
Connection ~ 1050 1350
Wire Wire Line
	1300 1400 1300 1350
Connection ~ 1300 1350
Wire Wire Line
	1050 1700 1050 1750
Connection ~ 1050 1750
Wire Wire Line
	1300 1700 1300 1750
Connection ~ 1300 1750
Wire Wire Line
	1550 1700 1550 1750
Connection ~ 1550 1750
Wire Wire Line
	1800 1700 1800 1750
Connection ~ 1800 1750
Wire Wire Line
	2100 1700 2100 1750
Connection ~ 2100 1750
Wire Wire Line
	3350 1400 3350 1350
Connection ~ 3350 1350
Wire Wire Line
	3600 1400 3600 1350
Connection ~ 3600 1350
Wire Wire Line
	3850 1400 3850 1350
Connection ~ 3850 1350
Wire Wire Line
	3100 1400 3100 1350
Connection ~ 3100 1350
Wire Wire Line
	2850 1400 2850 1350
Connection ~ 2850 1350
Wire Wire Line
	2600 1400 2600 1350
Connection ~ 2600 1350
Wire Wire Line
	2350 1400 2350 1350
Connection ~ 2350 1350
Wire Wire Line
	2100 1400 2100 1350
Connection ~ 2100 1350
Wire Wire Line
	1800 1400 1800 1350
Connection ~ 1800 1350
Wire Wire Line
	1550 1400 1550 1350
Connection ~ 1550 1350
NoConn ~ 6150 1650
Text HLabel 3650 3100 0    39   Input ~ 0
IOR_48
Text HLabel 3650 3150 0    39   Input ~ 0
IOR_49
Wire Wire Line
	3650 3100 3750 3100
Wire Wire Line
	3650 3150 3750 3150
Text HLabel 3650 3200 0    39   Input ~ 0
IOR_50
Text HLabel 3650 3250 0    39   Input ~ 0
IOR_51
Text HLabel 3650 3300 0    39   Input ~ 0
IOR_52
Text HLabel 3650 3350 0    39   Input ~ 0
IOR_53
Wire Wire Line
	3650 3200 3750 3200
Wire Wire Line
	3650 3250 3750 3250
Wire Wire Line
	3650 3300 3750 3300
Wire Wire Line
	3650 3350 3750 3350
$Comp
L D_Schottky D2
U 1 1 56A7C615
P 6050 1250
F 0 "D2" H 6050 1350 50  0000 C CNN
F 1 "D_Schottky" H 6050 1150 50  0000 C CNN
F 2 "Diodes_SMD:SOD-523" H 6050 1250 50  0001 C CNN
F 3 "" H 6050 1250 50  0000 C CNN
	1    6050 1250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6050 1400 6050 1650
Text HLabel 6050 600  0    60   Input ~ 0
P3V3
Wire Wire Line
	6050 600  6050 1100
Text HLabel 3650 1800 0    39   Output ~ 0
IOT-73
Wire Wire Line
	6550 3150 6700 3150
Text HLabel 6700 3200 2    39   Output ~ 0
IOL_1B
Wire Wire Line
	6550 3200 6700 3200
Text HLabel 6700 3250 2    39   Output ~ 0
IOL_2A
Wire Wire Line
	6550 3250 6700 3250
Text HLabel 6700 3300 2    39   Output ~ 0
IOL_2B
Wire Wire Line
	6550 3300 6700 3300
Text HLabel 6700 3350 2    39   Output ~ 0
IOL_3A
Wire Wire Line
	6550 3350 6700 3350
Text HLabel 6700 3400 2    39   Output ~ 0
IOL_3B
Wire Wire Line
	6550 3400 6700 3400
Text HLabel 6700 3450 2    39   Output ~ 0
IOL_4A
Wire Wire Line
	6550 3450 6700 3450
Wire Wire Line
	6550 3500 6700 3500
Text HLabel 6700 3550 2    39   Output ~ 0
IOL_5A
Wire Wire Line
	6550 3550 6700 3550
Wire Wire Line
	6550 3600 6700 3600
Text HLabel 6700 3650 2    39   Output ~ 0
IOL_6A
Wire Wire Line
	6550 3650 6700 3650
Wire Wire Line
	6550 3700 6700 3700
Text HLabel 6700 3750 2    39   Output ~ 0
IOL_7A
Wire Wire Line
	6550 3750 6700 3750
Wire Wire Line
	6550 3800 6700 3800
Text HLabel 6700 3850 2    39   Output ~ 0
IOL_8A
Wire Wire Line
	6550 3850 6700 3850
Wire Wire Line
	6550 3900 6700 3900
Text HLabel 6700 3950 2    39   Output ~ 0
IOL_9A
Wire Wire Line
	6550 3950 6700 3950
Wire Wire Line
	6550 4000 6700 4000
Text HLabel 3650 3400 0    39   Input ~ 0
IOR_54
Text HLabel 3650 3450 0    39   Input ~ 0
IOR_55
Wire Wire Line
	3650 3400 3750 3400
Wire Wire Line
	3650 3450 3750 3450
Text HLabel 6700 3500 2    39   Output ~ 0
IOL_4B
Text HLabel 6700 3600 2    39   Output ~ 0
IOL_5B
Text HLabel 6700 3700 2    39   Output ~ 0
IOL_6B
Text HLabel 6700 3800 2    39   Output ~ 0
IOL_7B
Text HLabel 6700 3900 2    39   Output ~ 0
IOL_8B
Text HLabel 6700 4000 2    39   Output ~ 0
IOL_9B
Text HLabel 7550 1450 2    60   Output ~ 0
HGND
Connection ~ 7350 1450
Text HLabel 7550 750  2    60   Output ~ 0
H3V3
Wire Wire Line
	3650 1800 3750 1800
Text HLabel 3650 1850 0    39   Output ~ 0
IOT-74
Text HLabel 3650 1900 0    39   Output ~ 0
IOT-75
Text HLabel 3650 1950 0    39   Output ~ 0
IOT-76
Text HLabel 3650 2000 0    39   Output ~ 0
IOT-77
Text HLabel 3650 2050 0    39   Output ~ 0
IOT-78
Text HLabel 3650 2100 0    39   Output ~ 0
IOT-79
Text HLabel 3650 2150 0    39   Output ~ 0
IOT-80
Text HLabel 3650 2200 0    39   Output ~ 0
IOT-81
Text HLabel 3650 2250 0    39   Output ~ 0
IOT-82
Text HLabel 3650 2300 0    39   Output ~ 0
IOT-83
Text HLabel 3650 2350 0    39   Output ~ 0
IOT-84
Text HLabel 3650 2400 0    39   Output ~ 0
IOT-85
Text HLabel 3650 2450 0    39   Output ~ 0
IOT-87
Text HLabel 3650 2500 0    39   Output ~ 0
IOT-88
Text HLabel 3650 2550 0    39   Output ~ 0
IOT-89
Text HLabel 3650 2600 0    39   Output ~ 0
IOT-90
Text HLabel 3650 2650 0    39   Output ~ 0
IOT-91
Wire Wire Line
	3650 1850 3750 1850
Wire Wire Line
	3650 1900 3750 1900
Wire Wire Line
	3650 1950 3750 1950
Wire Wire Line
	3650 2000 3750 2000
Wire Wire Line
	3650 2050 3750 2050
Wire Wire Line
	3650 2100 3750 2100
Wire Wire Line
	3650 2150 3750 2150
Wire Wire Line
	3650 2200 3750 2200
Wire Wire Line
	3650 2250 3750 2250
Wire Wire Line
	3650 2300 3750 2300
Wire Wire Line
	3650 2350 3750 2350
Wire Wire Line
	3650 2400 3750 2400
Wire Wire Line
	3650 2450 3750 2450
Wire Wire Line
	3650 2500 3750 2500
Wire Wire Line
	3650 2550 3750 2550
Wire Wire Line
	3650 2600 3750 2600
Wire Wire Line
	3650 2650 3750 2650
Text HLabel 7550 1550 2    60   Output ~ 0
H2GND
Text HLabel 1500 600  0    60   Input ~ 0
H1V2
Wire Wire Line
	1500 600  1650 600 
NoConn ~ 6550 1800
NoConn ~ 6550 1850
NoConn ~ 6550 1900
NoConn ~ 6550 1950
NoConn ~ 6550 2000
NoConn ~ 6550 2050
NoConn ~ 6550 2100
NoConn ~ 6550 2750
NoConn ~ 6550 2700
NoConn ~ 6550 2650
NoConn ~ 6550 2600
NoConn ~ 6550 2550
NoConn ~ 6550 2500
NoConn ~ 6550 2450
NoConn ~ 6550 2400
NoConn ~ 6550 2350
NoConn ~ 6550 2300
NoConn ~ 6550 2250
NoConn ~ 6550 2200
NoConn ~ 6550 2150
NoConn ~ 6550 4050
NoConn ~ 6550 4100
NoConn ~ 6550 4150
NoConn ~ 6550 4200
NoConn ~ 6550 4250
NoConn ~ 6550 4300
NoConn ~ 3750 4300
NoConn ~ 3750 4250
NoConn ~ 3750 3500
NoConn ~ 3750 3550
NoConn ~ 3750 3600
NoConn ~ 3750 3650
NoConn ~ 3750 2700
NoConn ~ 3750 2750
NoConn ~ 3750 2850
NoConn ~ 3750 2800
NoConn ~ 3750 2900
NoConn ~ 3750 3700
NoConn ~ 3750 3750
NoConn ~ 3750 3800
NoConn ~ 3750 3850
NoConn ~ 3750 3900
NoConn ~ 3750 3950
NoConn ~ 3750 4000
NoConn ~ 3750 4050
NoConn ~ 3750 4100
NoConn ~ 3750 4150
NoConn ~ 3750 4200
Text HLabel 6700 3150 2    39   Output ~ 0
IOL_1A
$Comp
L R R5
U 1 1 56B5700E
P 6900 4250
F 0 "R5" V 6980 4250 50  0000 C CNN
F 1 "10KΩ" V 6900 4250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 6830 4250 50  0001 C CNN
F 3 "" H 6900 4250 50  0000 C CNN
	1    6900 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 4100 7050 4100
Wire Wire Line
	7050 4100 7050 3950
Connection ~ 7450 3950
Wire Wire Line
	6900 4400 6900 4500
Connection ~ 6900 4500
Text Notes 7850 2700 0    60   ~ 0
SPI slave interface
Text Notes 7000 4700 0    60   ~ 0
Controls for SPI slave programming
Wire Wire Line
	7350 1050 7350 950 
Wire Wire Line
	7550 750  7450 750 
Wire Wire Line
	7450 750  7450 950 
Connection ~ 7450 950 
Wire Wire Line
	7350 1350 7350 1450
Wire Wire Line
	7550 1550 7450 1550
Wire Wire Line
	7450 1550 7450 1450
Connection ~ 7450 1450
$EndSCHEMATC
