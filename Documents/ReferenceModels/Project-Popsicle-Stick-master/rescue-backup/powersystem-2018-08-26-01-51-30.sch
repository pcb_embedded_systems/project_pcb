EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:STM32F07
LIBS:st_regul
LIBS:w_microcontrollers
LIBS:popsiclestick-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title "Project Popsicle Stick"
Date "2016-01-24"
Rev "1.0"
Comp ""
Comment1 "Author: Brosnan Yuen"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 3050 4400 0    60   Input ~ 0
USB_5V
Text HLabel 3100 5300 0    60   Input ~ 0
USB_GND
Wire Wire Line
	3100 5300 7400 5300
$Comp
L R R1
U 1 1 56A2661E
P 6500 4650
F 0 "R1" V 6580 4650 50  0000 C CNN
F 1 "66.5Ω" V 6500 4650 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 6430 4650 50  0001 C CNN
F 3 "" H 6500 4650 50  0000 C CNN
	1    6500 4650
	1    0    0    -1  
$EndComp
$Comp
L LED D1
U 1 1 56A26677
P 6500 5050
F 0 "D1" H 6500 5150 50  0000 C CNN
F 1 "LED" H 6500 4950 50  0000 C CNN
F 2 "LEDs:LED_0805" H 6500 5050 50  0001 C CNN
F 3 "" H 6500 5050 50  0000 C CNN
	1    6500 5050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6500 4800 6500 4850
Wire Wire Line
	6500 5250 6500 5300
Connection ~ 6500 5300
Text HLabel 7400 4400 2    60   Output ~ 0
P3V3
Text HLabel 7400 5300 2    60   Output ~ 0
PGND
$Comp
L C C1
U 1 1 56A26C1B
P 3400 4800
F 0 "C1" H 3425 4900 50  0000 L CNN
F 1 "0.011uF" H 3425 4700 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3438 4650 50  0001 C CNN
F 3 "" H 3400 4800 50  0000 C CNN
	1    3400 4800
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 56A26CB8
P 3900 4800
F 0 "C2" H 3925 4900 50  0000 L CNN
F 1 "0.1uF" H 3925 4700 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 3938 4650 50  0001 C CNN
F 3 "" H 3900 4800 50  0000 C CNN
	1    3900 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 4400 4400 4400
Wire Wire Line
	5200 4400 7400 4400
Wire Wire Line
	6500 4500 6500 4400
Connection ~ 6500 4400
$Comp
L C C6
U 1 1 56A271F4
P 5950 4800
F 0 "C6" H 5975 4900 50  0000 L CNN
F 1 "0.011uF" H 5975 4700 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 5988 4650 50  0001 C CNN
F 3 "" H 5950 4800 50  0000 C CNN
	1    5950 4800
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 56A271FA
P 5500 4800
F 0 "C4" H 5525 4900 50  0000 L CNN
F 1 "0.1uF" H 5525 4700 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 5538 4650 50  0001 C CNN
F 3 "" H 5500 4800 50  0000 C CNN
	1    5500 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 4650 3400 4400
Connection ~ 3400 4400
Wire Wire Line
	3900 4650 3900 4400
Connection ~ 3900 4400
Wire Wire Line
	3900 4950 3900 5300
Connection ~ 3900 5300
Wire Wire Line
	3400 4950 3400 5300
Connection ~ 3400 5300
Wire Wire Line
	5500 4650 5500 4400
Connection ~ 5500 4400
Wire Wire Line
	5950 4650 5950 4400
Connection ~ 5950 4400
Wire Wire Line
	5500 4950 5500 5300
Connection ~ 5500 5300
Wire Wire Line
	5950 4950 5950 5300
Connection ~ 5950 5300
Wire Wire Line
	5250 2800 7450 2800
$Comp
L LD1117S33CTR U1
U 1 1 56A1BAA9
P 4800 4450
F 0 "U1" H 4800 4700 50  0000 C CNN
F 1 "LD1117S33CTR" H 4800 4650 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-223" H 4800 4550 50  0001 C CNN
F 3 "" H 4800 4450 50  0000 C CNN
	1    4800 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 4700 4800 5300
Connection ~ 4800 5300
$Comp
L LD1117S12CTR U2
U 1 1 56A1BC87
P 4850 2850
F 0 "U2" H 4850 3100 50  0000 C CNN
F 1 "LD1117S12CTR" H 4850 3050 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-223" H 4850 2950 50  0001 C CNN
F 3 "" H 4850 2850 50  0000 C CNN
	1    4850 2850
	1    0    0    -1  
$EndComp
$Comp
L C C7
U 1 1 56A1BE9A
P 6000 3200
F 0 "C7" H 6025 3300 50  0000 L CNN
F 1 "0.011uF" H 6025 3100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 6038 3050 50  0001 C CNN
F 3 "" H 6000 3200 50  0000 C CNN
	1    6000 3200
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 56A1BEA0
P 5550 3200
F 0 "C5" H 5575 3300 50  0000 L CNN
F 1 "0.1uF" H 5575 3100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 5588 3050 50  0001 C CNN
F 3 "" H 5550 3200 50  0000 C CNN
	1    5550 3200
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 56A1C378
P 4250 3200
F 0 "C3" H 4275 3300 50  0000 L CNN
F 1 "0.1uF" H 4275 3100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 4288 3050 50  0001 C CNN
F 3 "" H 4250 3200 50  0000 C CNN
	1    4250 3200
	1    0    0    -1  
$EndComp
Text HLabel 3100 2800 0    60   Input ~ 0
USB_5V
Wire Wire Line
	4450 2800 3100 2800
Text HLabel 7450 2800 2    60   Output ~ 0
P1V2
Text HLabel 3100 3700 0    60   Input ~ 0
USB_GND
Wire Wire Line
	3100 3700 7450 3700
Wire Wire Line
	4250 3350 4250 3700
Connection ~ 4250 3700
Wire Wire Line
	4850 3100 4850 3700
Connection ~ 4850 3700
Wire Wire Line
	5550 3350 5550 3700
Connection ~ 5550 3700
Wire Wire Line
	6000 3350 6000 3700
Connection ~ 6000 3700
Text HLabel 7450 3700 2    60   Output ~ 0
PGND
Text Notes 4500 4100 0    118  ~ 0
3V3 RAIL
Wire Wire Line
	4250 3050 4250 2800
Connection ~ 4250 2800
Wire Wire Line
	5550 3050 5550 2800
Connection ~ 5550 2800
Wire Wire Line
	6000 3050 6000 2800
Connection ~ 6000 2800
Text Notes 4500 2450 0    118  ~ 0
1V2 RAIL
$EndSCHEMATC
