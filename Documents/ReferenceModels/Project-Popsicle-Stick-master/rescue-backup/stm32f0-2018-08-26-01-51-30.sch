EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:STM32F07
LIBS:st_regul
LIBS:w_microcontrollers
LIBS:popsiclestick-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title "Project Popsicle Stick"
Date "2016-01-24"
Rev "1.0"
Comp ""
Comment1 "Author: Brosnan Yuen"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 3550 1800 0    60   Input ~ 0
P3V3
$Comp
L STM32F070CBT6 U3
U 1 1 56A1DB5A
P 4850 2750
F 0 "U3" H 4700 2800 60  0000 C CNN
F 1 "STM32F070CBT6" H 5600 1750 60  0000 C CNN
F 2 "Housings_QFP:LQFP-48_7x7mm_Pitch0.5mm" H 4850 2750 60  0001 C CNN
F 3 "" H 4850 2750 60  0000 C CNN
	1    4850 2750
	1    0    0    -1  
$EndComp
$Comp
L C C8
U 1 1 56A1DC6C
P 2450 3600
F 0 "C8" H 2475 3700 50  0000 L CNN
F 1 "0.1uF" H 2475 3500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 2488 3450 50  0001 C CNN
F 3 "" H 2450 3600 50  0000 C CNN
	1    2450 3600
	1    0    0    -1  
$EndComp
Text HLabel 2450 3900 0    60   Input ~ 0
PGND
$Comp
L C C13
U 1 1 56A1DE52
P 4700 2150
F 0 "C13" H 4725 2250 50  0000 L CNN
F 1 "0.1uF" H 4725 2050 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 4738 2000 50  0001 C CNN
F 3 "" H 4700 2150 50  0000 C CNN
	1    4700 2150
	1    0    0    -1  
$EndComp
Text HLabel 4700 2400 0    60   Input ~ 0
PGND
Text HLabel 7700 2950 2    60   Input ~ 0
P3V3
$Comp
L C C15
U 1 1 56A1E3EE
P 7300 3300
F 0 "C15" H 7325 3400 50  0000 L CNN
F 1 "0.1uF" H 7325 3200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 7338 3150 50  0001 C CNN
F 3 "" H 7300 3300 50  0000 C CNN
	1    7300 3300
	1    0    0    -1  
$EndComp
Text HLabel 7300 3550 0    60   Input ~ 0
PGND
Text HLabel 7450 4450 2    60   Input ~ 0
P3V3
$Comp
L C C14
U 1 1 56A1E4B4
P 7050 4800
F 0 "C14" H 7075 4900 50  0000 L CNN
F 1 "0.1uF" H 7075 4700 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 7088 4650 50  0001 C CNN
F 3 "" H 7050 4800 50  0000 C CNN
	1    7050 4800
	1    0    0    -1  
$EndComp
Text HLabel 7050 5050 0    60   Input ~ 0
PGND
Text HLabel 4450 3650 0    60   Input ~ 0
PGND
Text HLabel 6050 5000 3    60   Input ~ 0
PGND
Text HLabel 5150 2050 1    60   Input ~ 0
PGND
Text HLabel 4050 3750 0    60   Input ~ 0
P3V3
$Comp
L C C12
U 1 1 56A1EB48
P 4100 4100
F 0 "C12" H 4125 4200 50  0000 L CNN
F 1 "0.1uF" H 4125 4000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 4138 3950 50  0001 C CNN
F 3 "" H 4100 4100 50  0000 C CNN
	1    4100 4100
	1    0    0    -1  
$EndComp
Text HLabel 4100 4400 0    60   Input ~ 0
PGND
Text HLabel 6700 3250 2    60   BiDi ~ 0
USBD+
Text HLabel 6700 3350 2    60   BiDi ~ 0
USBD-
$Comp
L Crystal Y1
U 1 1 56A25B79
P 3200 3350
F 0 "Y1" H 3200 3500 50  0000 C CNN
F 1 "32MHz" H 3200 3200 50  0000 C CNN
F 2 "custom:NX3225SA-32.000MHZ" H 3200 3350 50  0001 C CNN
F 3 "" H 3200 3350 50  0000 C CNN
	1    3200 3350
	1    0    0    -1  
$EndComp
$Comp
L C C9
U 1 1 56A25D07
P 2900 3650
F 0 "C9" H 2925 3750 50  0000 L CNN
F 1 "6pF" H 2925 3550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 2938 3500 50  0001 C CNN
F 3 "" H 2900 3650 50  0000 C CNN
	1    2900 3650
	1    0    0    -1  
$EndComp
$Comp
L C C11
U 1 1 56A25D89
P 3450 3650
F 0 "C11" H 3475 3750 50  0000 L CNN
F 1 "6pF" H 3475 3550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 3488 3500 50  0001 C CNN
F 3 "" H 3450 3650 50  0000 C CNN
	1    3450 3650
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH SW1
U 1 1 56A268C9
P 2800 5100
F 0 "SW1" H 2950 5210 50  0000 C CNN
F 1 "SW_PUSH" H 2800 5020 50  0000 C CNN
F 2 "custom:B3F-10XX" H 2800 5100 50  0001 C CNN
F 3 "" H 2800 5100 50  0000 C CNN
	1    2800 5100
	1    0    0    -1  
$EndComp
Text HLabel 2200 5650 0    60   Input ~ 0
PGND
Text HLabel 3400 4450 0    60   Input ~ 0
P3V3
$Comp
L C C10
U 1 1 56A26B94
P 3150 5350
F 0 "C10" H 3175 5450 50  0000 L CNN
F 1 "0.1uF" H 3175 5250 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 3188 5200 50  0001 C CNN
F 3 "" H 3150 5350 50  0000 C CNN
	1    3150 5350
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 56A26DA3
P 3400 4750
F 0 "R2" V 3480 4750 50  0000 C CNN
F 1 "10KΩ" V 3400 4750 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3330 4750 50  0001 C CNN
F 3 "" H 3400 4750 50  0000 C CNN
	1    3400 4750
	1    0    0    -1  
$EndComp
Text HLabel 5150 5000 3    60   Output ~ 0
SPI1NSS
Text HLabel 5250 5000 3    60   Output ~ 0
SPI1SCK
Text HLabel 5350 5000 3    60   Input ~ 0
SPI1MISO
Text HLabel 5450 5000 3    60   Output ~ 0
SPI1MOSI
Text HLabel 5950 5000 3    60   Output ~ 0
USART3RX
Text HLabel 5850 5000 3    60   Output ~ 0
USART3TX
Text HLabel 6150 2050 1    60   Output ~ 0
SWCLK
Text HLabel 6700 3150 2    60   Output ~ 0
SWDIO
Text HLabel 7700 3050 2    60   Output ~ 0
O3V3
Text HLabel 7700 3500 2    60   Output ~ 0
OGND
Wire Wire Line
	3550 1800 5050 1800
Wire Wire Line
	5050 1800 5050 2550
Wire Wire Line
	3650 1800 3650 2950
Connection ~ 3650 1800
Wire Wire Line
	2450 2950 2450 3450
Wire Wire Line
	2450 3750 2450 3900
Wire Wire Line
	4700 2400 4700 2300
Wire Wire Line
	4700 2000 4700 1800
Connection ~ 4700 1800
Wire Wire Line
	7300 3450 7300 3550
Wire Wire Line
	6550 2950 7700 2950
Connection ~ 7300 2950
Wire Wire Line
	7050 5050 7050 4950
Wire Wire Line
	6150 4450 7450 4450
Wire Wire Line
	7050 4650 7050 4450
Connection ~ 7050 4450
Wire Wire Line
	6050 4450 6050 5000
Wire Wire Line
	6550 3050 7100 3050
Wire Wire Line
	7100 3050 7100 3500
Wire Wire Line
	7100 3500 7700 3500
Connection ~ 7300 3500
Wire Wire Line
	5150 2050 5150 2550
Wire Wire Line
	4450 3650 4650 3650
Wire Wire Line
	4050 3750 4650 3750
Wire Wire Line
	4100 4250 4100 4400
Wire Wire Line
	6550 3250 6700 3250
Wire Wire Line
	6550 3350 6700 3350
Wire Wire Line
	2450 2950 4650 2950
Connection ~ 3650 2950
Wire Wire Line
	2900 3350 2900 3500
Wire Wire Line
	2900 3350 3050 3350
Wire Wire Line
	3350 3350 4650 3350
Wire Wire Line
	3450 3350 3450 3500
Connection ~ 3450 3350
Wire Wire Line
	4650 3450 2900 3450
Connection ~ 2900 3450
Wire Wire Line
	2450 3900 3450 3900
Wire Wire Line
	3450 3900 3450 3800
Wire Wire Line
	2900 3800 2900 3900
Connection ~ 2900 3900
Wire Wire Line
	4650 3550 3750 3550
Wire Wire Line
	3750 3550 3750 5100
Wire Wire Line
	3750 5100 3100 5100
Wire Wire Line
	2200 5100 2500 5100
Connection ~ 3150 5100
Wire Wire Line
	2200 5650 3150 5650
Wire Wire Line
	3150 5650 3150 5500
Wire Wire Line
	3150 5200 3150 5100
Wire Wire Line
	2200 5100 2200 5650
Wire Wire Line
	3400 4450 3400 4600
Wire Wire Line
	3400 4900 3400 5100
Connection ~ 3400 5100
Wire Wire Line
	6550 3950 6950 3950
Wire Wire Line
	6550 4050 6950 4050
Wire Wire Line
	5150 4450 5150 5000
Wire Wire Line
	5250 5000 5250 4450
Wire Wire Line
	5350 5000 5350 4450
Wire Wire Line
	5450 5000 5450 4450
Wire Wire Line
	5950 5000 5950 4450
Wire Wire Line
	5850 5000 5850 4450
Wire Wire Line
	6150 2050 6150 2550
Wire Wire Line
	6550 3150 6700 3150
Wire Wire Line
	7300 3050 7700 3050
Connection ~ 7300 3050
Text HLabel 4600 3850 0    60   Output ~ 0
PA0
Wire Wire Line
	4600 3850 4650 3850
Text HLabel 4600 3950 0    60   Output ~ 0
PA1
Text HLabel 4600 4050 0    60   Output ~ 0
PA2
Wire Wire Line
	4600 3950 4650 3950
Wire Wire Line
	4600 4050 4650 4050
Wire Wire Line
	4100 3950 4100 3750
Connection ~ 4100 3750
Text HLabel 5050 5000 3    60   Output ~ 0
PA3
Wire Wire Line
	5050 4450 5050 5000
$Comp
L R R3
U 1 1 56A3BA42
P 5450 1400
F 0 "R3" V 5530 1400 50  0000 C CNN
F 1 "100Ω" V 5450 1400 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5380 1400 50  0001 C CNN
F 3 "" H 5450 1400 50  0000 C CNN
	1    5450 1400
	-1   0    0    1   
$EndComp
Text HLabel 5550 5000 3    60   Output ~ 0
PB0
Wire Wire Line
	5550 4450 5550 5000
Wire Wire Line
	5650 4450 5650 5000
Text HLabel 5650 5000 3    60   Input ~ 0
PB1
Text HLabel 5750 5000 3    60   Output ~ 0
PB2
Wire Wire Line
	5750 4450 5750 5000
Text HLabel 5950 2050 1    60   Output ~ 0
PB3
Wire Wire Line
	5950 2550 5950 2050
Text HLabel 5850 2050 1    60   Output ~ 0
PB4
Text HLabel 5750 2050 1    60   Output ~ 0
PB5
Wire Wire Line
	5850 2550 5850 2050
Wire Wire Line
	5750 2550 5750 2050
Text HLabel 5450 1100 1    60   Input ~ 0
PGND
Wire Wire Line
	5550 2050 5550 2550
Wire Wire Line
	5650 2050 5650 2550
Wire Wire Line
	6550 3850 6950 3850
Text HLabel 6950 4050 2    60   Output ~ 0
SPI2NSS
Text HLabel 6950 3950 2    60   Output ~ 0
SPI2SCK
Text HLabel 6950 3850 2    60   Output ~ 0
SPI2MISO
Text HLabel 6950 3750 2    60   Output ~ 0
SPI2MOSI
Wire Wire Line
	6550 3750 6950 3750
Text HLabel 5550 2050 1    60   Output ~ 0
I2C1_SDA
Text HLabel 5650 2050 1    60   Output ~ 0
I2C1_SCL
Wire Wire Line
	5450 1100 5450 1250
Wire Wire Line
	5450 1550 5450 2550
Text HLabel 7700 2850 2    60   Output ~ 0
H3V3
Text HLabel 7700 3600 2    60   Output ~ 0
HGND
Text HLabel 5250 2050 1    60   Output ~ 0
PB9
Text HLabel 5350 2050 1    60   Output ~ 0
PB8
Wire Wire Line
	5250 2550 5250 2050
Wire Wire Line
	5350 2550 5350 2050
Text HLabel 6050 2050 1    60   Output ~ 0
PA15
Wire Wire Line
	6050 2550 6050 2050
Text HLabel 6700 3450 2    60   Output ~ 0
PA10
Wire Wire Line
	6550 3450 6700 3450
Text HLabel 6700 3550 2    60   Output ~ 0
PA9
Text HLabel 6700 3650 2    60   Output ~ 0
PA8
Wire Wire Line
	6550 3550 6700 3550
Wire Wire Line
	6550 3650 6700 3650
Text HLabel 4600 3050 0    60   Output ~ 0
PC13
Wire Wire Line
	4600 3050 4650 3050
Text HLabel 4600 3150 0    60   Output ~ 0
PC14
Text HLabel 4600 3250 0    60   Output ~ 0
PC15
Wire Wire Line
	4600 3150 4650 3150
Wire Wire Line
	4600 3250 4650 3250
Wire Wire Line
	7600 3500 7600 3600
Wire Wire Line
	7600 3600 7700 3600
Connection ~ 7600 3500
Text Notes 2550 4850 0    60   ~ 0
Reset button
Wire Wire Line
	7300 3150 7300 2950
Wire Wire Line
	7700 2850 7400 2850
Wire Wire Line
	7400 2850 7400 2950
Connection ~ 7400 2950
$EndSCHEMATC
