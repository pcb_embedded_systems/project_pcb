# [Project-Popsicle-Stick] (https://hackaday.io/project/9380-project-popsicle-stick)

ICE40 + STM32 development board. STM32F070CBT6 + ICE40HX1K-TQ144

![Screenshot](https://github.com/brosnanyuen/Project-Popsicle-Stick/blob/master/popsiclestick.jpg)

![Screenshot2](https://github.com/brosnanyuen/Project-Popsicle-Stick/blob/master/popsiclestick2.jpg)

![Screenshot3](https://github.com/brosnanyuen/Project-Popsicle-Stick/blob/master/popsiclestick3.jpg)


## Introduction

Popsicle stick is a simple FPGA development board. The schematics and PCBs are open hardware are free to use in any other project. The board is meant to be easily soldered using a hot air gun. The components are cheap and have lots of stock. ICE40HX1K-TQ144 (FPGA) is the heart of the project and has a small amount of logic gates. This makes the FPGA perfect for beginners. STM32F070CBT6 (MCU) is used to configure and aid the FPGA. The MCU has 128 KB of flash and can store bit streams. The MCU has many interfaces and ADC. ADC can be used to read analog signals and relay it to the FPGA. The motivation behind the project is to learn how to do PCB design.


## Main components:

*   STM32F070CBT6
*   ICE40HX1K-TQ144
*   5V Micro USB connector
*   Crystal oscillators
*   Linear voltage regulators


## Specification:

*    Power from 5V Micro USB
*    Program STM32 using USB 2.0 or STLink
*    Configure FPGA from STM32 using SPI
*    36 IO pins for FPGA
*    17 IO pins for STM32
*    I2C
*    SPI
*    UART/USART
*    Support IceStorm
*    Cost $40 USD <


## System Design


![Screenshot5](https://i.imgur.com/fMA8TX0.png)


The STM32 is programmed using SWD (header pin) or micro USB. Then the MCU configures ICE40 using SPI. Once configuration is complete, the ICE40 will able to communicate with STM32 using IO. ICE40 is directly connected to 36 header pins. The STM32 has I2C, SPI, UART/USART, and ADC. With the power of ICE40 and STM32 you can drive motors, LEDs and sample analog signals.





